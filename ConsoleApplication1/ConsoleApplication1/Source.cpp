#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <istream>
#include<sstream>



using namespace std;

vector<vector<string>> STORED_DATA;// all data stored at all 

/**
* @class CSROW
* This class implements the course. It is a detailed comment
*/
class CSROW
{
	vector<string> data;
public:
	size_t size() const{ return data.size(); }

	void readNextRow(istream& str){

		string line;
		getline(str, line);
		stringstream lineStream(line);
		string cell;
		data.clear();
		while (getline(lineStream, cell, ',')){ data.push_back(cell); }

		STORED_DATA.push_back(data);


	}
};

/**
* This function is the main function
* 
*
* @param[in] no parameters
* @param[out] returns 0
* @return True if successful, FALSE otherwise
*/

int main() {

	ifstream file;
	file.open("C:\\Users\\Tarmah Kopite\\Desktop\\SCHOOL1.csv");      // hard coded path

	if (!file){ cout << "could not open file"; } //check if file doesnt open
	else{
		cout << "File Opened \n";
		CSROW a;

		while (!file.eof()){ a.readNextRow(file); } ///Reading data 

		for (int i = 0; i < STORED_DATA.size(); i++)  ///writing all data
		{
			for (int j = 0; j < STORED_DATA[i].size(); j++)
			{
				cout << STORED_DATA[i][j] << ",";
			}
			cout << "\n";
		}

		cout << "\n\n\n";
		cout << "names of schools are as follows \n\n";  //task 3
		for (int i = 6; i < STORED_DATA.size() - 5; i++)
		{

			if (STORED_DATA[i][8] == "0"){ cout << STORED_DATA[i][1]; cout << "\n"; }

		}

		cout << "\n\n";  //task 4
		float totalSchools = 0; float largeSchools = 0;
		float percentage;
		int x = 0;
		int b, max = 0;
		string name;

		for (int i = 6; i < STORED_DATA.size() - 1; i++)
		{
			x = stoi(STORED_DATA[i][6]);

			if (x > 50){ largeSchools++;      b = stoi(STORED_DATA[i][12]); if (b > max){ max = b; name = STORED_DATA[i][1]; } }

			totalSchools++;
		}

		percentage = (largeSchools / totalSchools) * 100;
		cout << "Percetage = " << percentage << "%\n";
		cout << "name of school = " << name << endl;;


		file.close();
	}
}
